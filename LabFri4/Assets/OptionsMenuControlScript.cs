﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionsMenuControlScript : MonoBehaviour {
    [SerializeField] Dropdown _camera;
    [SerializeField] Toggle _toggleMusic;
    [SerializeField] Toggle _toggleSFX;
    [SerializeField] Button _backButton;
    // Start is called before the first frame update
    void Start () {
        _camera.value = GameApplicationManager.Instance.DifficultyLevel;
        _toggleMusic.isOn = GameApplicationManager.Instance.MusicEnabled;
        _toggleSFX.isOn = GameApplicationManager.Instance.SFXEnabled;
        _camera.onValueChanged.AddListener (delegate {
            cameraChanged (
                _camera);
        });
        _toggleMusic.onValueChanged.AddListener (delegate { OnToggleMusic (_toggleMusic); });
        _toggleSFX.onValueChanged.AddListener (delegate { OnToggleSFX (_toggleSFX); });
        _backButton.onClick.AddListener (delegate { BackButtonClick (_backButton); });
    }

    // Update is called once per frame
    void Update () {

    }
    public void BackButtonClick (Button button) {
        SceneManager.UnloadSceneAsync ("SceneOptions");
       
        GameApplicationManager.Instance.IsOptionMenuActive = false;
    }

    public void cameraChanged (Dropdown dropdown) {
        GameApplicationManager.Instance.DifficultyLevel = dropdown.value;
    }

    public void OnToggleMusic (Toggle toggle) {
        GameApplicationManager.Instance.MusicEnabled = _toggleMusic.isOn;
    }
    public void OnToggleSFX (Toggle toggle) {
        GameApplicationManager.Instance.SFXEnabled = _toggleSFX.isOn;
    }
}

